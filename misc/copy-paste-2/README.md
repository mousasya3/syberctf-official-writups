# SYberCTF – copy paste 2

* **Category:** misc
* **Points:** 40 points

## Challenge

the flag must be around here somewhere right ?!

> Author: b4n4n4s

## Solution

ok for this one you need a little bit of hard work 😅

hmmm i think the most logical place for the flag to be placed is right where the sentence : `the flag must be around here somewhere right ?!` is placed

so maybe its hidden 🤔

u know this thingy called `inspect element` in most browsers that shows the html code that is used to display this element

well when we try using that we see that the flag is actually hidden in there in a html comment

![](Screenshot1.png)

well it is a copy paste after all right ?

```
flag{50und5_l1k3_c0p9p4573_bu7_w1th_3x7r4_573p5}
```