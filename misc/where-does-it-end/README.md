# SYberCTF – where does it end ?

* **Category:** misc
* **Points:** 150 points

## Challenge

![](https://images2.memedroid.com/images/UPLOADED24/519edb3c0afc5.jpeg)

> Author: b4n4n4s, t0mcr8se, Radwan

## Attachments

[basedzip.txt](./basedzip.txt)

## Hint

rick rolled ?

lol noob

jk try curl

## Solution

ok the hint doesn't help that much until you got rick rolled

now for the first step (yeah this challenge do have a lot of steps)

we open the `basedzip.txt` then we realize that it is a base64 encoded file (from our past experience in this CTF)

ok the easy way to decode this file is to upload it to ![cyberchef](https://gchq.github.io/CyberChef/)

and I see the magic wand button so i click it to detect the file type of the output and it seems to be a zip file (as the name of the file suggested)

so I downloaded that file to ![download.zip](./download.zip)

then when unzipping the file we get a new mp3 file called `song.mp3`, hmmmm its a popular meme song `HEYYEYAAEYAAAEYAEYAA` but wait there is someone talking at the end of the song

I guess its some kind of password, lets write it down : `p4s5w0rd_l0l`

hmmm put a password for what I must ask..

lets continue investigating, lets try strings on the song : 

![](Screenshot1.png)

oh wow, noice a [url](https://drive.google.com/file/d/1gWsP-4mPEcX8qEw6LMJIU6SOxgVnnTWY/view?usp=sharing) to a google drive file lets open it : 

![](Screenshot2.png)

oh no it needs a pass-, wait I have one lol (`p4s5w0rd_l0l`) lets try it..

cool it worked but what is this strange pdf ?!

`68747470733a2f2f676574666c61672e303030776562686f73746170702e636f6d2f`

it looks like a hex lets unhexify it (using cyberchef again) : 

`https://getflag.000webhostapp.com/`

another url ?!!

really where does it end ?

hopefuly this is the end since it says getflag in the url so lets hope

![](Screenshot3.png)

I have no words -_-

really a rick roll in 2020 ??!!!

ok here is where the hint come in handy

lets try curling the url :

![](Screenshot4.png)

oh great finally it ends here... or does it ? *vsauce theme intensifies*

```
flag{0h_f1n4lly_1_h4ve_r34ch3d_7h3_3nd_______0r_H4v3_I_?!}
```