# SYberCTF – is this challenge broken ?

* **Category:** misc
* **Points:** 100 points

## Challenge

well the challenge here is how to get rid of the infinite `lol` alerts

## Solution

hmmm, so first how to get rid of the annoying alerts, just open a new tab lol

now how to open the challenge without triggering the alerts

so first we know that the alert function is a javascript function

sooo, we can just override that javascript function with something else that isn't annoying as much

yah you can change it to anything else

but how do you do that you ask ?

its simple just go to the developers console in the browser you are using and execute this command : `alert = 0` which actually just makes the alert function an integer and it would not be able to call it

so we try opening the challenge...

yay no more annoying alerts that lols you in the face (lol)

![](Screenshot1.png)

```
flag{H0w_d1D_y0u_f1nd_m3}
```