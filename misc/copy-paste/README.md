# SYberCTF – copy paste

* **Category:** misc
* **Points:** 10 points

## Challenge

flag{kn0w_y0ur_k39b04rd_5h0r7cu75}

> Author: b4n4n4s

## Solution

select the flag with your mouse

do `ctrl+c` to copy the flag

then do `ctrl+v` to paste the flag in the submit flag section


```
flag{kn0w_y0ur_k39b04rd_5h0r7cu75}
```