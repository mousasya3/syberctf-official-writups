# SYberCTF – sanity check

* **Category:** misc
* **Points:** 25 points

## Challenge

have you ever heard of discord ?

join us :

https://discord.gg/h3s2csq

## Solution

yah actually all you had to do is join discord using the link above

if you don't have a discord account or don't know what discord is just google it

after you've joined our discord go to the misc channel to find an interesting message in the description

`!flag for free flag`

yah just type `!flag` in the chat and our bot `MEE6` will send it to you


```
flag{s4n1ty_ch3ck_101}
```