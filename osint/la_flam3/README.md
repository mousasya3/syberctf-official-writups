# SYberCTF – la_flam3

* **Category:** osint
* **Points:** 75 points

## Challenge

la_flam3 has an important account on one of the social networks out there

where he used to discuss his issues with friends and other people from the world

can you find that account (I think he left a flag there)

> Author: b4n4n4s

## Hint

https://youtu.be/_VRyoaNF9sk?t=71

## Solution

ok it is not as simple as searching on google

la flame himself gave you a hint `you need more than google just to find me`

ok lets find him using other search tools specifically username search tools like [namechk](https://namechk.com/)

so we put his username `la_flam3` there and press search

we see he doesn't have many accounts we just go and try every single one and look for the flag

and if you are sm0rt enough it says in the description that he used to "discuss" so lets try the disqus account :

![](Screenshot1.png)

```
flag{d15_C7F_1s_l1t}
```