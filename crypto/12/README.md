# SYberCTF – base

* **Category:** crypto
* **Points:** 50 points

## Challenge

no not the police

`♏♈♎♉♋♈♉♐`

note: put the secret message (all lowercase) in the flag format : `flag{}`

> Author: b4n4n4s (inspired by 241n)

## Solution

hmmm what are these symbols lets google one of them

oh they are zodiac signs it seems lemme put there names in order :

```
Scorpius
Aries
Libra
Taurus
Cancer
Aries
Taurus
Sagittarius
```

do you notice smth ?

lets put the first letter of each name together : `saltcats`

so we try it and yeah it worked, ( I think this one is easier for yall than base lol)

```
flag{saltcats}
```