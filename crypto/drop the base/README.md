# SYberCTF – drop the base

* **Category:** crypto
* **Points:** 62 points

## Challenge

1cHeuEW6TQgg05NbZhFxdfRWUeDrPAe2r93ba7jonni3B

> Author: b4n4n4s

## Hint

https://youtu.be/QQcQDbpDH_o?t=75

## Solution

if you solved the first base challenge I think this one is a piece a cake for you

the hint is just for fun nothing to do with the challenge (I think its a nice reference don't u ?)

ok so drop the base logicaly thinking its a base64 lets try it...

hmm nonsense lets rewind the name of the challenge is drop the base so lets try lowering the base number for example 62 (taking the number of points as a hint)

and oops I got the flag 

```
flag{w3ll_7h47_35c4l473d_qu1ckl9}
```