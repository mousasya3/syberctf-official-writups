# SYberCTF – base

* **Category:** crypto
* **Points:** 38 points

## Challenge

base what ?!

`ZmxhZ3tZalJ6TTE5ek1YaDBPVjltTUhWeX0=`

> Author: b4n4n4s

## Solution

ok I know you don't even know what cryptography is so here is a simple one

we can tell by the trailing `=` sign that this string is in base64 format so we look for a base64 decoder online like [this one](https://www.base64decode.org/)

so we just paste the string there and get the flag Y4y!!

wait wtd (what the duck if you're wondering) it doesn't seem like a flag : `flag{YjRzM19zMXh0OV9mMHVy}`

maybe the inside is also in base64 it seems like it

aha nailed it !

```
flag{b4s3_s1xt9_f0ur}
```