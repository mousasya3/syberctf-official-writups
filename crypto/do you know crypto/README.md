# SYberCTF – do you know crypto

* **Category:** crypto
* **Points:** 100 points

## Challenge

`--- -... ...-- --. .-- ....- .-.. ...-- --. ..-. .--. .-- --- -- .--- ...- --- .--- .--. - -.-. -..- ...-- ...- .--. .- --.. .-- --- -..- ...-- -. -- .. ....- -..- ..- --.. -... --.- .-.. ....- ..... -.-. --.- --... .. -...-`

> Author: b4n4n4s

## Hint

I cooked this one

## Solution

ok its obviously morse code but its too long to decode manually

(taking the hint into consideration I suggest using [cyberchef](https://gchq.github.io/CyberChef/) for this)

ok we get this :
`OB3GW4L3GFPWOMJVOJPTCX3VPAZWOX3NMI4XUZBQL45CQ7I=`

looks like a base64 but didnt work maybe another base ooh look a magic wand appeared in the cyberchef interface lets click on it

neat its base32

`pvkq{1_g15r_1_ux3g_mb9zd0_:(}`

ok now what to do with that ?

it looks like the flag format but I guess the letters are messed up

I guess ROT13 is the logical cipher for this

playing with the offset a little bit we found out that it is `16`

```
flag{1_w15h_1_kn3w_cr9pt0_:(}
```