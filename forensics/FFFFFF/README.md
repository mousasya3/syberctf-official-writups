# SYberCTF – FFFFFF

* **Category:** forensics
* **Points:** 100 points

## Challenge

Oh no...

I guess i messed up the bytes in my screenshot

now it won't open

can you help me?

> Author: b4n4n4s

## Attachments

[flag.png](flag.png)

## Solution

what ?? the image is corrupt ?!

hmmm lets try fixing it

lets open it with a hex editor to see whats wrong :

![](screenshot1.png)

hmmm I guess the first 3 bytes are wrong they must spell out `PNG`

lets fix the file's header by looking up the correct one from [wikipedia](https://en.wikipedia.org/wiki/Portable_Network_Graphics) :

![](./screenshot2.png)

ok so the correct first 3 bytes are : `89504E` lets try fixing it

![](./flag_fixed.png)

```
flag{D4rk_m0d3}
```