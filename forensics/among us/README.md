# SYberCTF – among us

* **Category:** forensics
* **Points:** 50 points

## Challenge

I think a certain someone is sus

can you find out who ?

> Author: b4n4n4s

## Attachments

[image.jpg](./image.jpg)

![](image.jpg)

## Solution

hmmm a flag among us ?

the first thing comes in mind is that the flag is inside the image somewhere so we try the `strings` command with `grep` to find the flag: `strings image.jpg | grep flag` 

![](screenshot1.png)

another way to solve it is using `exiftool` command which shows the metadata of the image :

![](screenshot2.png)

```
flag{3x1f700l5_15_5u5}
```