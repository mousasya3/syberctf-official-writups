# SYberCTF – learnpwn

* **Category:** pwn
* **Points:** 150 points

## Challenge

WTH is a shellcode ?!

`nc syberctf.hadara-group.com 1337`

> Author: b4n4n4s

## Hint

https://youtu.be/HSlhY4Uy8SA

## Attachments

[learnpwn](./learnpwn)

## Solution

ok assuming you watched the video and know what is a shellcode proceed with this writeup

if you still don't know what a shellcode is go and do some more research before coming here

ok lets run the binary that we have (which the same as the nc) :

![](screenshot1.png)

hmmm ok ez we just need to find a suitable shellcode to pass it to the binary

ok first thing we need to determine the architecture of our binary by running `file learnpwn` :

![](screenshot2.png)

ok cool it seems a linux x86-64 ELF

now we go to [shellstorm](http://shell-storm.org/shellcode/) to find a suitable shellcode

we scroll down to linux/x86-64 and we look for a `bin/sh` shellcode like [this one](http://shell-storm.org/shellcode/files/shellcode-806.php)

`\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05`

ok if you try to pass that as is to the binary as an input it'll not work we have to echo it to the binary so `\xff` get recognized as a `ff` byte not as literal chars so we try this command :

![](screenshot3.png)

what ?! what do you mean by done ? well at least it didn't cause a segfault so I guess the shellcode is working properly but why the hell did he exit ?

ok I think I know the solution lets just put the shell commands in the echo string so we can execute them for example to get the flag :

![](screenshot4.png)

ok awseome it showed my testing flag so all we have to do is run it again but this time with the nc command to get the real flag :

![](screenshot5.png)

```
flag{L00k_m0Mmy_1_pWn3d_th3_s9St3m}
```