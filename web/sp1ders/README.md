# SYberCTF – sp1ders

* **Category:** web
* **Points:** 50 points

## Challenge
I like robots, I'd love to be a robot one day

>link: http://94.141.219.18:8005/

>author t0mcr8se
## hints
a file that tells search engine crawlers which pages or files the crawler can or can't request from your site.
## Solution
```
$ curl http://94.141.219.18:8005/
yeah so there's nothing here<br>
if you are a robot, you know what to do ...

```
so we go to `robots.txt`
```
$ curl http://94.141.219.18:8005/robots.txt
User-Agent: *
Disallow: /trash/102.html
```
go to `/trash/102.html`
```
$ curl http://94.141.219.18:8005/trash/102.html
<!-- /trash/108.html -->
```
and then to `108.html` and so on until we reach the last page. Basically the numbers are ascii values of characters of the flag